<?php
//fetches buyer info from the db, then passes that to easypost and expects to
// receive rates, label, or address validity
namespace EasyPost;

use Connections\DatabaseQuery;
use EasyPost\Mode\KeyRetriever;
use EasyPost\ReturnLabel\ReturnLabel;
use Exception;

class EasyPostController {
  public $fromAddress;
  public $toAddress;
  public $parcelInfo; //Weights and dims
  protected $carrier;
  protected $service;
  public $insurance; //this will get written in
  public $orderId;
  public $customsInfo;
  public $isReturn;

  /**
   * Take all possible request for information for EasyPost put it in the right
   * format, and pass it on to createShipment
   *
   * @param (String) the person who is shipping the package
   * @param (String) the orderId
   * @param (String) what is wanted from EasyPost
   * @param (Boolean) Is signature confirmation necessary for this parcel
   * @param (String) Set to USPS, UPS, Fedex, DHL, etc.
   * @param (String) The type of shipment you want from the above carrier i.e. overnight
   * @return (Shipment)
   */
  public function indexAction ($builder, $orderId, $action, $confirmation, $carrier = null, $service = null) {
    $this->carrier = $carrier;
    $this->service = $service;
    $this->orderId = $orderId;
    KeyRetriever::indexAction();
    EasyPostFromAddress::indexAction($builder, $this); //Uses the builder's name to go and get his address from the db
    EasyPostToAddress::create($orderId, $this); //Uses the orderId to go and get the clients address from the db
    if (empty($this->parcelInfo)) {
      EasyPostParcelInfo::indexAction($orderId, $this); //Gets weights and dims from db using orderId //Also write to insurance!!
    }
    ReturnLabel::isReturn($orderId, $this); //Gets weights and dims from db using orderId //Also write to insurance!!
    return $this->createShipment($confirmation, $action);
  }
  /**
   * Create the shipment and then call the $action to get the information needed
   * about the shipments
   *
   * @param (Boolean) Was signature confirmation chosen
   * @param (String) The method that this method should call
   * @return (Shipment)
   */
  public function createShipment ($confirmation, $action) {
    // Instantiates the shipment object and sends in the toAddress, fromAddress and Parcel information (weight and dims)
    if (isset($_SESSION['customsInfo'])) {
      $customsSession = $_SESSION['customsInfo'];
      $this->customsInfo = unserialize(base64_decode($customsSession));
    }
    $shipmentArray = [
      "to_address" => $this->toAddress,
      "from_address" => $this->fromAddress,
      "parcel" => $this->parcelInfo,
      "isReturn"=> $this->isReturn,
      "options"=> ['label_format' => 'PNG']
    ];
    if ($this->toAddress->country != "US") {
      $shipmentArray["customs_info"] = $this->customsInfo;
    }
    if ($confirmation == 1) {
      $shipmentArray['options']['delivery_confirmation'] = "SIGNATURE";
    }
    if ($this->isReturn == 1) {
      $shipmentArray['is_return'] = true;
    }

    try {
      $shipment = Shipment::create($shipmentArray);
    } catch (Exception $e) {
      echo "If this package is shipping within the United States, the address_country needs to be set to 'United States'";
    }

    //the call_user_func calls the function name that is sent in the $action
    // var (either "buy", "verify" or "rates") and for params it sends the
    // already instantiated $shipment object, along with the id of a shipping
    // method in case the action is "buy". If the method that is going to be
    // called isn't $this->buy the $id won't be used
    $result = call_user_func([$this, $action], $shipment);
    $this->intoSession($shipment);
    return $result;
  }

  /**
   * @param $shipment (Shipment) Chuck everything into the session
   */
  public function intoSession ($shipment) {
    $_SESSION['mode'] = $shipment->mode;
    $_SESSION['toAddress'] = [];
    $_SESSION['toAddress']['name'] = $shipment->to_address->name;
    $_SESSION['toAddress']['line1'] = $shipment->to_address->street1;
    $_SESSION['toAddress']['city'] = $shipment->to_address->city;
    $_SESSION['toAddress']['state'] = $shipment->to_address->state;
    $_SESSION['toAddress']['zip'] = $shipment->to_address->zip;
    $_SESSION['fromAddress'] = [];
    $_SESSION['fromAddress']['name'] = $shipment->from_address->company;
    $_SESSION['fromAddress']['line1'] = $shipment->from_address->street1;
    $_SESSION['fromAddress']['city'] = $shipment->from_address->city;
    $_SESSION['fromAddress']['state'] = $shipment->from_address->state;
    $_SESSION['fromAddress']['zip'] = $shipment->from_address->zip;
  }
  //If the requester only wants rates for a package this method will be called by call_user_func
  protected function rates (Shipment $shipment) {
    $i = 0;
    //After I sent the toAddress, fromAddress and parcelInfo to instatiate the $shipment object, the rates were available
    //Now I am just saving the rates into an array to show the requester
    $shipment->get_rates(); //Run the get_rates function from the shipment object
    foreach ($shipment->rates as $rate) {
      $rateOptions[$i]['carrier'] = ($rate->carrier);
      $rateOptions[$i]['services'] = ($rate->service);
      $rateOptions[$i]['rate'] = ($rate->rate);
      $rateOptions[$i]['id'] = ($rate->id);
      $i++;
    }
    return $rateOptions;
  }
  //I need to $shipment object and the $id which has the rate Id in it.
  protected function buy (Shipment $shipment) {
    $shipment->buy($shipment->lowest_rate([$this->carrier], [$this->service]));
    $this->shipmentId($shipment->id);
    $this->insuranceAction($shipment);
    return $shipment;
  }
  //Testing to see if the address is valid
  protected function verify () {
    if ($this->toAddress == "Address Not Found.") {
      return $this->toAddress;
    }
    return "";
  }
  protected function price (Shipment $shipment) {
    $labelValue = $shipment->lowest_rate([$this->carrier], [$this->service]);
    return $labelValue->rate;
  }
  protected function shipmentId ($shipmentId) {
    if (isset($this->orderId)) {
      $connection = new DatabaseQuery("ODbackoffice");
      $connection->run("UPDATE ejunkieData SET shipmentId = :shipmentId WHERE orderId = :orderId", [":shipmentId"=>"$shipmentId", ":orderId"=>"$this->orderId"]);
    }
  }
  protected function insuranceAction (Shipment $shipment) {
    if ($this->insurance == "") {
      return;
    } else {
      $shipment->insure(['amount' => $this->insurance]);
      return;
    }
  }
}
