<?php

namespace PackingSlip\ShopDrawing;

require_once dirname(dirname(dirname(__DIR__))) . "/vendor/dompdf/dompdf/autoload.inc.php";
use Connections\DatabaseQuery;
use Lib\Authentication;
use Lib\ShellObject\Item;
use Dompdf\Dompdf;

/**
 * Class ShopDrawingController
 * @package PackingSlip\ShopDrawing : This manages the calling of the shop drawings
 * for all the products except the cubby grid
 */
class ShopDrawingController extends Authentication {
  public $orderId;
  public $addressName;
  public $quantity = 0;
  public $numUsed = 0;
  public $inserts = [];
  public $insertNumber = 0;

  /**
   * @param $orderId
   * @param $offset
   * @return string
   */
  public function pdf ($orderId, $offset = 0) {
    $this->orderId = $orderId;
    $this->indexAction($orderId);
    //Find out what type of an item it is, and then go get the shop drawing
    $dompdf = new Dompdf();
    $shopDrawing = "";
    foreach ($this->inserts as $key=>$value) {
      switch ($value->productName) {
        case "Custom Insert":
          $shopDrawing .= $this->customInsert($value, 1);
          break;
        case "Santiago Knife Block":
          $drawing = new SantiagoKnifeBlockShopDrawingView();
          $shopDrawing .= $drawing->indexAction($value, $this->addressName);
          break;
        case "Cubby Grid":
          $shopDrawing .= "";
          break;
        default:
          $drawing = new KnifeBlockShopDrawingView();
          $shopDrawing .= $drawing->indexAction($value, $this->addressName);
          break;
      }
    }
    $dompdf->loadHtml($shopDrawing);
    $dompdf->render();
    $name = str_replace(' ', '', $this->addressName);
    $dompdf->stream($name . "dwg");
    return '';
  }
  public function web ($orderId, $offset) {
    $this->indexAction($orderId);
    //Find out what type of an item it is, and then go get the shop drawing
    //Eventually, once the pdf is working, there will be a foreach here that will get the separate pdfs and send them out together
    switch ($this->inserts[$offset]->productName) {
      case "Custom Insert":
        $shopDrawing = $this->customInsert($this->inserts[$offset]);
        break;
      case "Santiago Knife Block":
        $shopDrawing = new SantiagoKnifeBlockShopDrawingView();
        $shopDrawing = $shopDrawing->indexAction($this->inserts[$offset], $this->addressName);
        break;
      case "Cubby Grid":
        $shopDrawing = "";
        break;
      default:
        $shopDrawing = new KnifeBlockShopDrawingView();
        $shopDrawing = $shopDrawing->indexAction($this->inserts[$offset], $this->addressName); //This can be for the maple Knife Block
        break;
    }
    return $shopDrawing;
  }
  public function indexAction ($orderId) {
    $connection = new DatabaseQuery("ODbackoffice");
    $result = $connection->run("SELECT num_cart_items, addressName FROM ejunkieData WHERE orderId = :orderId", [":orderId"=>$orderId]);
    $numCartItems = $result[0]['num_cart_items']; //first find the number of inserts in this order
    $this->addressName = $result[0]['addressName'];
    for ($i = 1; $i <= $numCartItems; $i++) { //Loop through the NumCartItems getting the item of each
      $result = $connection->run("SELECT item$i FROM ejunkieData WHERE orderId = :orderId", [":orderId"=>$orderId]);
      $item[$i] = unserialize(base64_decode($result[0]["item" . $i])); //parse the item into an object
      $this->quantity = $this->quantity+$item[$i]->quantity; //Save the quantity of that item
    }
    foreach($item as $singleItem) { //Looping through all the items so as to calculate the number of the one that is wanted (defined by offset)
      array_push($this->inserts, $this->insertNumbers($singleItem)); //InsertNumbers will return me
    }
  }

  /**
   * @param $item
   * @return mixed
   */
  public function insertNumbers ($item) {
    //$this->numUsed will keep track of the insert number that we're on and find it's number code
    $this->insertNumber++;
    $item->num = "Insert ";
    for ($i = 0; $i < $item->quantity; $i++) { // Loop through the number of times
      $this->numUsed++; //increase the number we're on
      if (strlen($item->num) > 7) { //If the num already has an item number in it
        $item->num .= " & " . $this->numUsed;
      } else {
        $item->num .= $this->numUsed;
      }
    }
    $item->num .= " (of $this->quantity)";
    return $item;
  }
  public function customInsert (Item $item, $pdf = 0) {
    $insert = $item;
    $insert->ht = $item->dimensions->height;
    $insert->targeted = $item->targeted;
    $insert->fixed = $item->fixed;
    $insert->btm = $item->bottom;
    if ($insert->btm == "wbtm") {
      $insert->btm = "Attached Bottom";
    }
    $insert->q = $item->quantity;
    $insert->d = $this->flipToFraction($item->dimensions->depth);
    $insert->w = $this->flipToFraction($item->dimensions->width);
    $insert->dd = $item->dimensions->depth+.25;
    $insert->dw = $item->dimensions->width+.25;
    $insert->name = $item->name;
    if (isset($item->name) && $item->name != "") {
      $insert->name = $item->name;
    }

    $dims = array_change_key_case((array)$item->dims, CASE_LOWER);
    $_SESSION['seven']['values'] = $dims;

    $insert->totalDivs = 0;
    foreach ($item->divsInSec as $key => $value) {
      $_SESSION['seven']['values']['divs'][strtolower($key)] = $value;
      $insert->totalDivs = $insert->totalDivs + $value;
    }

    if (isset($item->scoopsInSec)) {
      $insert->totalScoops = 0;
      foreach ($item->scoopsInSec as $key => $value) {
        $_SESSION['seven']['values']['scoops'][strtolower($key)] = $value;
        $insert->totalScoops = $insert->totalScoops+$value;
      }
    }
    $_SESSION['four']['d'] = $item->dimensions->depth;
    $_SESSION['four']['w'] = $item->dimensions->width;
    $insert->design = $item->design;

    $wizardConnection = new DatabaseQuery("ODwizard");
    $result = $wizardConnection->run("SELECT * FROM templates WHERE name = :name", [":name" => $insert->design]);
    if (empty($result)) {
      return "";
    }

    $template = [
      'basicdata' => [
        'templateid' => $result[0]['templateid'],
        'templateName' => $result[0]['name'],
        'corners' => $result[0]['dcorn'],
        'numOfSecs' => $result[0]['sec'],
        'numOfSlottedSecs' => $result[0]['slotsec'],
        'minDepth' => $result[0]['minD'],
        'minWidth' => $result[0]['minW'],
      ],
      'dims' => [
        'a' => [
          'type' => $result[0]['Atype'],
          'formula' => $result[0]['A'],
          'start' => $result[0]['Astart'],
        ],
        'b' => [
          'type' => $result[0]['Btype'],
          'formula' => $result[0]['B'],
          'start' => $result[0]['Bstart'],
        ],
        'c' => [
          'type' => $result[0]['Ctype'],
          'formula' => $result[0]['C'],
          'start' => $result[0]['Cstart'],
        ],
        'd' => [
          'type' => $result[0]['Dtype'],
          'formula' => $result[0]['D'],
          'start' => $result[0]['Dstart'],
        ],
        'e' => [
          'type' => $result[0]['Etype'],
          'formula' => $result[0]['E'],
          'start' => $result[0]['Estart'],
        ],
        'f' => [
          'type' => $result[0]['Ftype'],
          'formula' => $result[0]['F'],
          'start' => $result[0]['Fstart'],
        ],
        'g' => [
          'type' => $result[0]['Gtype'],
          'formula' => $result[0]['G'],
          'start' => $result[0]['Gstart'],
        ],
        'h' => [
          'type' => $result[0]['Htype'],
          'formula' => $result[0]['H'],
          'start' => $result[0]['Hstart'],
        ],
        'i' => [
          'type' => $result[0]['Itype'],
          'formula' => $result[0]['I'],
          'start' => $result[0]['Istart'],
        ],
        'j' => [
          'type' => $result[0]['Jtype'],
          'formula' => $result[0]['J'],
          'start' => $result[0]['Jstart'],
        ],
      ],
      'secs' => [
        's1' => [
          'corners' => $result[0]['s1corn'],
          'divDirection' => $result[0]['divdir1'],
          'edgesFromClockwise' => $result[0]['sec1edges'],
          'vertDimIs' => $result[0]['tiein1v'],
          'horizDimIs' => $result[0]['tiein1h'],
        ],
        's2' => [
          'corners' => $result[0]['s2corn'],
          'divDirection' => $result[0]['divdir2'],
          'edgesFromClockwise' => $result[0]['sec2edges'],
          'vertDimIs' => $result[0]['tiein2v'],
          'horizDimIs' => $result[0]['tiein2h'],
        ],
        's3' => [
          'corners' => $result[0]['s3corn'],
          'divDirection' => $result[0]['divdir3'],
          'edgesFromClockwise' => $result[0]['sec3edges'],
          'vertDimIs' => $result[0]['tiein3v'],
          'horizDimIs' => $result[0]['tiein3h'],
        ],
        's4' => [
          'corners' => $result[0]['s4corn'],
          'divDirection' => $result[0]['divdir4'],
          'edgesFromClockwise' => $result[0]['sec4edges'],
          'vertDimIs' => $result[0]['tiein4v'],
          'horizDimIs' => $result[0]['tiein4h'],
        ],
        's5' => [
          'corners' => $result[0]['s5corn'],
          'divDirection' => $result[0]['divdir5'],
          'edgesFromClockwise' => $result[0]['sec5edges'],
          'vertDimIs' => $result[0]['tiein5v'],
          'horizDimIs' => $result[0]['tiein5h'],
        ],
        's6' => [
          'corners' => $result[0]['s6corn'],
          'divDirection' => $result[0]['divdir6'],
          'edgesFromClockwise' => $result[0]['sec6edges'],
          'vertDimIs' => $result[0]['tiein6v'],
          'horizDimIs' => $result[0]['tiein6h'],
        ],
      ],
      'ctrlLines' => [
        'c1' => [
          'formu' => $result[0]['c1val'],
          'dir' => $result[0]['C1dir'],
        ],
        'c2' => [
          'formu' => $result[0]['c2val'],
          'dir' => $result[0]['C2dir'],
        ],
        'c3' => [
          'formu' => $result[0]['c3val'],
          'dir' => $result[0]['C3dir'],
        ],
        'c4' => [
          'formu' => $result[0]['c4val'],
          'dir' => $result[0]['C4dir'],
        ],
        'c5' => [
          'formu' => $result[0]['c5val'],
          'dir' => $result[0]['C5dir'],
        ],
        'c6' => [
          'formu' => $result[0]['c6val'],
          'dir' => $result[0]['C6dir'],
        ],
      ]
    ];
    $connection = new DatabaseQuery("ODbackoffice");
    if ($pdf == 1) {
      $shopDwg = new CustomInsertShopDrawingPdf($insert, $template);
      $dateTime = date("Y-m-d H:i:s");
      $connection->run("UPDATE ejunkieData SET download = :datetime WHERE orderId = :orderId", array(":datetime"=>$dateTime, ":orderId"=>$this->orderId));
    } else {
      $shopDwg = new CustomInsertShopDrawingView($insert, $template);
    }
    $shopDwg = $shopDwg->indexAction($this->addressName);
    return $shopDwg;
  }

  /**
   * @param $dim
   * @return string
   */
  public function flipToFraction ($dim) {
    $num = explode(".", $dim);
    $first = $num[0];
    if (empty($num[1])) {
      $number = 0;
    } else {
      $number = floatval("." . $num[1]);
    }
    switch(true) {
      case ($number < .016):
        $fraction = "";
        break;
      case ($number < .047):
        $fraction = "1/32";
        break;
      case ($number < .078):
        $fraction = "1/16";
        break;
      case ($number < .109):
        $fraction = "3/32";
        break;
      case ($number < .141):
        $fraction = "1/8";
        break;
      case ($number < .172):
        $fraction = "5/32";
        break;
      case ($number < .203):
        $fraction = "3/16";
        break;
      case ($number < .234):
        $fraction = "7/32";
        break;
      case ($number < .266):
        $fraction = "1/4";
        break;
      case ($number < .297):
        $fraction = "9/32";
        break;
      case ($number < .328):
        $fraction = "5/16";
        break;
      case ($number < .359):
        $fraction = "11/32";
        break;
      case ($number < .391):
        $fraction = "3/8";
        break;
      case ($number < .422):
        $fraction = "13/32";
        break;
      case ($number < .453):
        $fraction = "7/16";
        break;
      case ($number < .484):
        $fraction = "15/32";
        break;
      case ($number < .516):
        $fraction = "1/2";
        break;
      case ($number < .547):
        $fraction = "17/32";
        break;
      case ($number < .578):
        $fraction = "9/16";
        break;
      case ($number < .609):
        $fraction = "19/32";
        break;
      case ($number < .641):
        $fraction = "5/8";
        break;
      case ($number < .672):
        $fraction = "21/32";
        break;
      case ($number < .703):
        $fraction = "11/16";
        break;
      case ($number < .734):
        $fraction = "23/32";
        break;
      case ($number < .766):
        $fraction = "3/4";
        break;
      case ($number < .797):
        $fraction = "25/32";
        break;
      case ($number < .828):
        $fraction = "13/16";
        break;
      case ($number < .859):
        $fraction = "27/32";
        break;
      case ($number < .891):
        $fraction = "7/8";
        break;
      case ($number < .922):
        $fraction = "29/32";
        break;
      case ($number < .953):
        $fraction = "15/16";
        break;
      case ($number < .984):
        $fraction = "31/32";
        break;
      case ($number <= .99999999):
        $fraction = "";
        $first = $first+1;
        break;
      default:
        $fraction = "";
        break;
    }
    return $first . " " . $fraction;
  }
}